olimexisp.name=AVR ISP 500 Olimex  
olimexisp.communication=serial
olimexisp.protocol=stk500v2
olimexisp.program.protocol=stk500v2
olimexisp.program.tool=avrdude
olimexisp.program.extra_params=-P{serial.port}
